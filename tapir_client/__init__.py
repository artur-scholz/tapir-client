import datetime

import requests
from requests.auth import HTTPBasicAuth


class TapirClient:

    def __init__(self, host, port):
        if not isinstance(port, int):
            raise TypeError("<port> must be an instance of int")
        self.host = host
        self.port = port
        self.url = "{}:{}".format(host, port)
        self.auth_url = self.url + "/api/auth/"
        self.token = None

    def login(self, user, password):
        auth = HTTPBasicAuth(user, password)
        r = requests.get(self.auth_url + "token", auth=auth)
        if r.status_code != 200:
            return False
        self.token = r.json()['token']
        return True

    def logout(self):
        r = requests.get(self.auth_url + "logout")
        if r.status_code != 200:
            return False
        self.token = None
        return True

    def datastore(self):
        return Datastore(self)


class Datastore:

    def __init__(self, client):
        self.client = client
        self.url = client.url + "/api/datastore/"

    def load_documents(
            self, domain, model, filter=None, fields=None,
            sort=None, limit=None, page=None, pagesize=None):
        documents = self.find(
            domain, model, filter, fields, sort, limit, page, pagesize)
        return documents

    def save_documents(self, domain, model, documents, replace=False):
        if replace:
            self.delete(domain, model)
        return self.insert(domain, model, documents)

    def load_dataframe(
            self, domain, model, time_fields=None, filter=None, fields=None,
            sort=None, limit=None, page=None, pagesize=None):
        import pandas as pd
        documents = self.find(
            domain, model, filter, fields, sort, limit, page, pagesize)
        if documents is None:
            return None
        df = pd.DataFrame(documents)
        if time_fields:
            for time_field in time_fields:
                df[time_field] = pd.to_datetime(df[time_field])
        return df

    def save_dataframe(self, domain, model, df, replace=False):
        if replace:
            self.delete(domain, model)
        return self.insert_df(domain, model, df)

    def delete_model(self, domain, model):
        return self.delete(domain, model)

    def insert(self, domain, model, documents):
        if not isinstance(documents, list):
            documents = [documents]
        r = requests.post(
            self.url + domain + '/' + model, json=documents,
            headers={'Token': self.client.token})
        return r.status_code == 201

    def insert_df(self, domain, model, dataframe):
        return self.insert(domain, model, dataframe.to_dict(orient='records'))

    def find(
            self, domain, model, filter=None, fields=None, sort=None,
            limit=None, page=None, pagesize=None):
        # filtering
        _filter = ""
        if filter:
            if not isinstance(filter, dict):
                raise ValueError('filter must be an instance of dict')
            for key, value in filter.items():
                if isinstance(value, dict):
                    for k, v in value.items():
                        if not isinstance(v, list):
                            value = "{}:{}".format(k, v)
                        else:
                            v = str(v)[1:-1]
                            v = v.replace(" ", "")
                            value = "{}:{}".format(k, v)
                        _filter += "{}={}&".format(key, value)
                elif isinstance(value, list):
                    _value = str(value)[1:-1]
                    value = _value.replace(" ", "")
                    _filter += "{}={}&".format(key, value)
                else:
                    value = str(value)
                    _filter += "{}={}&".format(key, value)
            if _filter.endswith('&'):
                _filter = _filter[:-1]

        # projection
        _fields = ""
        if fields:
            if not isinstance(fields, list):
                raise ValueError('fields must be an instance of list')
            values = ','.join(fields)
            _fields = '_fields={}'.format(values)

        # sorting
        _sort = ""
        if sort:
            if not isinstance(sort, list):
                raise ValueError('sort must be an instance of list')
            values = ','.join(sort)
            _sort = '_sort={}'.format(values)

        # limiting and paging
        _limit = ""
        _page = ""
        _pagesize = ""
        if limit:
            _limit = "_limit={}".format(limit)
        elif page:
            _page = "_page={}".format(page)
            if pagesize:
                _pagesize = "_pagesize={}".format(pagesize)

        # build the query string
        query = ""
        if _filter:
            query += _filter + '&'
        if _fields:
            query += _fields + '&'
        if _sort:
            query += _sort + '&'
        if _page:
            query += _page + '&'
        if _pagesize:
            query += _pagesize + '&'
        if _limit:
            query += _limit + '&'

        if query:
            query = '?' + query
        if query.endswith('&'):
            query = query[:-1]

        r = requests.get(
            self.url + domain + '/' + model + query,
            headers={'Token': self.client.token})
        if r.status_code != 200:
            return None
        return r.json()

    def replace(self, domain, model, _id, document):
        if isinstance(document, list):
            document = document[0]
        for key, value in document.items():
            if isinstance(value, datetime.date):
                document[key] = value.isoformat()
        try:
            r = requests.put(
                self.url + domain + '/' + model + '/' + str(_id),
                json=document, headers={'Token': self.client.token})
        except Exception:
            return False
        return r.status_code == 200

    def modify(self, domain, model, _id, document):
        if isinstance(document, list):
            document = document[0]
        for key, value in document.items():
            if isinstance(value, datetime.date):
                document[key] = value.isoformat()
        try:
            r = requests.patch(
                self.url + domain + '/' + model + '/' + str(_id),
                json=document, headers={'Token': self.client.token})
        except Exception:
            return False
        return r.status_code == 200

    def delete(self, domain, model, filter=None):
        """Deletes documents from the model.

        :param filter: Filter to apply. Set to ``None`` to match all.
        :rtype: bool
        """
        # filtering
        _filter = ""
        if filter:
            if not isinstance(filter, dict):
                raise ValueError('filter must be an instance of dict')
            for key, value in filter.items():
                if isinstance(value, dict):
                    for k, v in value.items():
                        if not isinstance(v, list):
                            value = "{}:{}".format(k, v)
                        else:
                            v = str(v)[1:-1]
                            v = v.replace(" ", "")
                            value = "{}:{}".format(k, v)
                        _filter += "{}={}&".format(key, value)
                elif isinstance(value, list):
                    _value = str(value)[1:-1]
                    value = _value.replace(" ", "")
                    _filter += "{}={}&".format(key, value)
                else:
                    value = str(value)
                    _filter += "{}={}&".format(key, value)
            if _filter.endswith('&'):
                _filter = _filter[:-1]

        # build the query string
        query = ""
        if _filter:
            query = '?' + _filter

        r = requests.delete(
            self.url + domain + '/' + model + query,
            headers={'Token': self.client.token})
        if r.status_code != 200:
            return False
        return True
