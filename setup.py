from setuptools import setup

setup(
    name='tapir-client',
    version='0.0.1',
    packages=['tapir_client'],
    install_requires=['requests'],
)
